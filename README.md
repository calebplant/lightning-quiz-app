# Lightning Quiz App

## Overview

A lightning web component that allows users to take quizzes and review their results.

## Demo (~1:40 min)

[Here's a link to a demo video for the component](https://www.youtube.com/watch?v=hGzhPIK8VYI)

## Some Screenshots

### Register Pop Up

![Register Pop Up](media/register.png)

### Quiz In Progress

![Quiz In Progress](media/quiz.png)

### Graded Quiz

![Graded Quiz](media/quiz-complete.png)

## Functional Overview

### Files

#### LWC

* **QuizAppComponent** - Main component that contains both QuizAppRegisterForm and QuizAppBody. Handles controller communication and passing down of data.
* **QuizAppRegisterForm** - Form for selecting the question set to use, and popping up the modal for registering the participant's email.
* **QuizAppBody** - Displays the actual quiz. Contains a list of QuizAppQuestions to show each question.
* **QuizAppQuestion** - Displays a question and its answer options. Used by QuizAppBody.
* **QuizAppSummary** - Displays the results after submitting the quiz (time taken, number correct, score)

#### Apex

* **QuizAppController** - Component controller. Handles finding/creating Quiz Participants, fetching questions to return for the quiz, and shuffling the order of questions and recording it.
* **QuizResponse** - Wrapper class for building the controller's response to a request for questions

### Schema

![Quiz Schema](media/schema.png)

To prevent replicating the same data, Quiz Questions set their answer options as lookups to a separate Quiz Answer object. To grant quesiton flexibility, up to 5 answer options can be added, but fewer work just fine. Quiz Questions are associated to a Quiz Question Set via a junction Quiz Questions in Set object. Finally, Quiz Sessions are generated based on the pairing of the Quiz Participant and Quiz Question Set. They record the question order the user last experienced and will update when the order is shuffled upon a retake.