import { api, LightningElement } from 'lwc';

export default class QuizAppQuestion extends LightningElement {
    @api questionData;
    @api participantAnswers;
    @api checkedAnswers;

    get answerOptions() {
        return this.questionData.answerOptions.map(eachAnswer => {
                return {label: eachAnswer.label, value: eachAnswer.id}
        });
    }

    get questionText() {
        return this.questionData.questionNumber + '. ' + this.questionData.label;
    }
    
    get selectedAnswer() {
        if(this.participantAnswers[this.questionData.id] === null) {
            return undefined;
        }
        return this.participantAnswers[this.questionData.id];
    }

    get iconName() {
        return this.checkedAnswers[this.questionData.id] ? 'action:approval': 'action:close';
    }

    get rowClass() {
        let result = 'slds-m-vertical_small';
        if(this.checkedAnswers) {
            result += this.checkedAnswers[this.questionData.id] ? ' green-shade' : ' red-shade';
        }
        return result;
    }

    get isReadOnly() {
        return this.checkedAnswers ? true : false;
    }

    // Handlers
    handleChange(event) {
        console.log('quizAppQuestion :: handleChange');
        // this.selectedAnswer = event.target.value;
        let params = { 
            questionId: this.questionData.id,
            answerId: event.target.value
        };
        this.dispatchEvent(new CustomEvent('answerchange', { detail: params}));
    }
}