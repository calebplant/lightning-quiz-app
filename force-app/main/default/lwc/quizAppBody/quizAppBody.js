import { api, LightningElement } from 'lwc';

export default class QuizAppBody extends LightningElement {
    @api
    get questions() {
        return this._questions;
    }
    set questions(value) {
        this.setAttribute('questions', value);
        this._questions = value;
        this.updatePagination();
    }

    @api participantAnswers;
    @api checkedAnswers;

    showResetModal = false;
    questionsPerPage = 10;
    totalPages;
    currentPage;

    previousSliderValue;
    currentSliderValue;
    throttleSliderUpdate;

    // Private
    _questions;
    _timerInterval;
    _timeElapsed = 0;

    get displayedQuestions()
    {
        let start = Number(this.currentPage) * Number(this.questionsPerPage);
        let end = Number(start) + Number(this.questionsPerPage);
        return this.questions.slice(start, end);
    }

    get nextDisabled() {
        return (this.currentPage == (this.totalPages - 1) || this.currentPage == undefined);
    }

    get previousDisabled() {
        return (this.currentPage === 0 || this.currentPage == undefined);
    }

    get timeElapsed() {
        return `${Math.floor(this._timeElapsed / 60)} : ${(this._timeElapsed % 60).toString().padStart(2, '0')}`;
    }

    connectedCallback() {
        console.log('connected callback');
        this._timerInterval = setInterval(function () {
            this._timeElapsed++;
        }.bind(this), 1000);
    }

    @api stopTimer() {
        console.log('quizAppBody :: stopTimer');
        window.clearInterval(this._timerInterval);
        this._timerInterval = null;
    }

    // Handlers
    handleReset(event) {
        console.log('quizAppBody :: handleReset');
        this.showResetModal = true;
    }

    handleConfirmReset(event) {
        console.log('quizAppBody :: handleConfirmReset');
        this._timeElapsed = 0;
        if(!this._timerInterval) {
            this._timerInterval = setInterval(function () {
                this._timeElapsed++;
            }.bind(this), 1000);
        }
        this.dispatchEvent(new CustomEvent('resetquiz'));
        this.showResetModal = false;
    }

    handleCancelReset(event) {
        console.log('quizAppBody :: handleCancelReset');
        this.showResetModal = false;
    }

    handleSubmit(event) {
        console.log('quizAppBody :: handleSubmit');
        this.dispatchEvent(new CustomEvent('submitquiz'));
    }

    handleAnswerChange(event) {
        console.log('quizAppBody :: handleAnswerChange');
        this.dispatchEvent(new CustomEvent('answerchange', {detail: event.detail}));
    }

    handleQuestionsPerPageChange(event) {
        this.currentSliderValue = event.target.value;
        if(!this.throttleSliderUpdate) {
            this.throttleSliderUpdate = true;
            this.waitForSliderStop();
        }
    }

    handlePageChange(event) {
        console.log('quizAppBody :: handlePageChange');
        switch(event.currentTarget.name){
            case 'previous':
                this.currentPage--;
                break;
            case 'next':
                this.currentPage++;
                break;
        }
    }

    handleTryOther(event) {
        console.log('quizAppBody :: handleTryOther');
        this.dispatchEvent(new CustomEvent('tryother'));
    }

    // Utils
    waitForSliderStop() {
        if (this.previousSliderValue === this.currentSliderValue) {
            this.questionsPerPage = this.previousSliderValue;
            this.previousSliderValue = undefined;
            this.currentSliderValue = undefined;
            this.throttleSliderUpdate = false;
            this.updatePagination();
        } else {
            this.previousSliderValue = this.currentSliderValue;
            setTimeout(this.waitForSliderStop.bind(this), 700);
        }
    }

    updatePagination() {
        this.currentPage = 0;
        this.totalPages = Math.ceil(this.questions.length / this.questionsPerPage);
    }
}