import { LightningElement, wire } from 'lwc';
import { createRecord } from 'lightning/uiRecordApi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { refreshApex } from '@salesforce/apex';
import getQuestionSets from '@salesforce/apex/QuizAppController.getAvailableQuestionSets';
import getExistingParticipant from '@salesforce/apex/QuizAppController.checkForExistingParticipant';
import fetchQuestions from '@salesforce/apex/QuizAppController.fetchQuestions';
import checkAnswers from '@salesforce/apex/QuizAppController.checkAnswers';

import PARTICIPANT_OBJECT from '@salesforce/schema/Quiz_Participant__c';
import NAME_FIELD from '@salesforce/schema/Quiz_Participant__c.Name';
import EMAIL_FIELD from '@salesforce/schema/Quiz_Participant__c.Email__c';

export default class QuizAppComponent extends LightningElement {

    questionSets;
    selectedQuestionSet;
    previousQuestionSet;
    inputEmail;
    payload;
    participantId;
    questionData;
    participantAnswers = {};
    checkedAnswers;

    // Private
    _questionsResponse;

    @wire(getQuestionSets)
    getQuestionSets(response) {
        if(response.data) {
            console.log('Received question sets from server:');
            console.log(response.data);
            this.questionSets = response.data;
        }
        if(response.error) {
            console.log('Error receiving question set from server!');
            console.log(response.error);
        }
    }

    @wire(fetchQuestions, {req: '$payload'})
    getQuestions(response) {
        this._questionsResponse = response;
        if(response.data) {
            console.log('Received questions from server:');
            console.log(response.data);
            response.data.questions.forEach(eachQuestion => {
                this.participantAnswers[eachQuestion.id] = null;
            });
            this.questionData = response.data;
        }
        if(response.error) {
            console.log('Error receiving questions from server!');
            console.log(response.error);
        }
    }

    get showRegisterForm() {
        return this.questionSets && !this.questionData;
    }

    // Handlers
    handleQuestionSetChange(event) {
        console.log('QuizAppComponent :: handleQuestionSetChange');
        this.selectedQuestionSet = event.detail;
    }

    handleSubmitEmail(event) {
        console.log('QuizAppComponent :: handleSubmitEmail');

        // Check if participant already set and is taking another quiz
        if(this.participantId && this.selectedQuestionSet) {
            if(this.previousQuestionSet == this.selectedQuestionSet) {
                refreshApex(this._questionsResponse);
            } else {
                this.payload = {
                    participantId: this.participantId,
                    questionSetId: this.selectedQuestionSet
                };
            }
            return;
        }

        getExistingParticipant({email: event.detail})
        .then(result => {
            if(result) {
                console.log('Found existing participant.');
                console.log(result);
                this.participantId = result;
                this.payload = {
                    participantId: this.participantId,
                    questionSetId: this.selectedQuestionSet
                };
                this.inputEmail = event.detail;
            } else {
                console.log('No existing participant found.');

                const fields = {};
                fields[NAME_FIELD.fieldApiName] = 'placeholder';
                fields[EMAIL_FIELD.fieldApiName] = event.detail;
                const recordInput = { apiName: PARTICIPANT_OBJECT.objectApiName, fields};
                createRecord(recordInput)
                .then(result => {
                    console.log('Created new participant');
                    console.log(result);
                    this.payload = {
                        participantId: result.id,
                        questionSetId: this.selectedQuestionSet
                    };
                    console.log('PAYLOAD: ');
                    console.log(JSON.parse(JSON.stringify(this.payload)));
                })
                .catch(error => {
                    console.log('Error creating new participant!');
                    console.log(error);
                })
            }
        })
        .catch(error => {
                console.log('Error searching for existing participant!');
                console.log(error);
            })
    }

    handleSubmitQuiz(event) {
        console.log('QuizAppComponent :: handleSubmitQuiz');

        // Check if every answer is filled in
        let unansweredQuestions = [];
        Object.keys(this.participantAnswers).forEach((key, index) => {
            if(this.participantAnswers[key] == null) {
                unansweredQuestions.push(index + 1);
            }
        });
        if(unansweredQuestions.length > 0) {
            this.dispatchEvent(new ShowToastEvent({
                title: 'Answer Every Question',
                message: 'Please answer the following questions: ' + unansweredQuestions.join(', '),
                variant: 'error'
            }));
            return;
        }

        // Check answers on backend
        checkAnswers({
            answersString: JSON.stringify(this.participantAnswers)
        })
        .then(result => {
            console.log('Successfully submitted answers');
            console.log(result);
            this.checkedAnswers = result;
            this.template.querySelector('c-quiz-app-body').stopTimer();
        })
        .catch(error => {
            console.log('Error sending answers!');
            console.log(error);
        });
    }

    handleResetQuiz(event) {
        console.log('QuizAppComponent :: handleResetQuiz');
        this.checkedAnswers = undefined;
        refreshApex(this._questionsResponse);
        this.dispatchEvent(new ShowToastEvent({
            title: 'Quiz Reset',
            message: 'Your quiz was reset.',
            variant: 'success'
        }));
    }

    handleAnswerChange(event) {
        console.log('QuizAppComponent :: handleAnswerChange');
        // @api doesnt detect individual property changes, so have to set the entire object
        let newAnswers = {...this.participantAnswers};
        newAnswers[event.detail.questionId] = event.detail.answerId;
        this.participantAnswers = newAnswers;
    }

    handleTryOther(event) {
        console.log('QuizAppComponent :: handleTryOther');
        this.previousQuestionSet = this.selectedQuestionSet;
        this.checkedAnswers = undefined;
        this.participantAnswers = {};
        this.questionData = undefined;
        this.selectedQuestionSet = undefined;
    }
}