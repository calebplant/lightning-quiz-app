import { api, LightningElement } from 'lwc';

export default class QuizAppSummary extends LightningElement {

    @api checkedAnswers;
    @api timeElapsed;

    get amountCorrectLabel() {
        let total = 0;
        let correct = 0;
        Object.keys(this.checkedAnswers).forEach(key => {
            if(this.checkedAnswers[key] === true) {
                correct++;
            }
            total++;
        })
        return `${correct} / ${total}`;
    }

    get scoreLabel() {
        let total = 0;
        let correct = 0;
        Object.keys(this.checkedAnswers).forEach(key => {
            if(this.checkedAnswers[key] === true) {
                correct++;
            }
            total++;
        });
        return `${(correct / total * 100).toFixed(2)}%`;
    }

    // Handlers
    handleTryAgain(event) {
        console.log('quizAppSummary :: handleTryAgain');
        this.dispatchEvent(new CustomEvent('reset'));
    }

    handleTryOther(event) {
        console.log('quizAppSummary :: handleTryOther');
        this.dispatchEvent(new CustomEvent('tryother'));
    }
}