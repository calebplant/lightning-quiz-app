public with sharing class QuizAppController {
    
    /*
        Returns all available Question Sets for the user to choose from
    */
    @AuraEnabled(cacheable=true)
    public static List<Quiz_Question_Set__c> getAvailableQuestionSets()
    {
        System.debug('START getAvailableQuestionSets');
        return [SELECT Id, Name FROM Quiz_Question_Set__c
                ORDER BY Name DESC];
    }

    /*
        Returns a Quiz_Participant Id associated with the passed email. If no match is found, returns null.

        Parameters:
        @   email - Email used to lookup the correct Quiz_Participant
    */
    @AuraEnabled(cacheable=true)
    public static Id checkForExistingParticipant(String email)
    {
        System.debug('START checkForExistingParticipant');
        if(email != null) {
            List<Quiz_Participant__c> participant = [SELECT Id FROM Quiz_Participant__c WHERE Email__c = :email LIMIT 1];
            return participant.size() > 0 ? participant[0].Id : null;
        }
        return null;
    } 

    /*
        Returns the list of questions that will be presented in the quiz

        Parameters:
        @   req - QuizRequest containing the Question_Set Id and Quiz_Participant Id
    */
    @AuraEnabled(cacheable=true)
    public static QuizResponse fetchQuestions(QuizRequest req)
    {
        System.debug('START fetchQuestions');
        System.debug(req);

        if(req.questionSetId == null) {
            return null;
        }

        // Get Questions
        List<Quiz_Question__c> questionPool = getQuestionPool(req.questionSetId);

        // Get Question Order
        List<Integer> newQuestionOrder = new List<Integer>();
        List<Quiz_Session__c> existingSession = getExistingSession(req.participantId, req.questionSetId);
        if(existingSession.size() > 0) {
            // If there's an existing session...
            List<Integer> previousQuestionOrder = parseQuestionOrder(existingSession[0].Question_Order__c);
            newQuestionOrder = generateQuestionOrder(questionPool.size());
            // Make sure previous order != new order
            ensureIsNewOrder(previousQuestionOrder, newQuestionOrder);
            // Update session order
            existingSession[0].Question_Order__c = encodeQuestionOrder(newQuestionOrder);
            update existingSession[0];
        } else {
            // Generate question order
            newQuestionOrder = generateQuestionOrder(questionPool.size());
            // Create session
            Quiz_Session__c newSession = new Quiz_Session__c(
                Quiz_Participant__c = req.participantId,
                Question_Set__c = req.questionSetId,
                Question_Order__c = encodeQuestionOrder(newQuestionOrder)
            );
        }

        // Build questions for response
        List<Quiz_Question__c> returnedQuestions = new List<Quiz_Question__c>();
        for(Integer eachInt : newQuestionOrder) {
            returnedQuestions.add(questionPool[eachInt]);
        }

        QuizResponse result = new QuizResponse(returnedQuestions);
        return result;
    }

    /*
        Returns a Map<Id, Boolean> of each graded question Id and whether it was correct (True) or incorrect (False)

        Parameters:
        @   answerString - String containing a JSON object that posseses a series of parameters like so:
                            { [QuestionId] : [UserAnswerId]}
    */
    @AuraEnabled(cacheable=true)
    public static Map<Id, Boolean> checkAnswers(String answersString)
    {
        System.debug('START checkAnswers');
        System.debug(answersString);

        Map<Id, Id> answerIdByQuestionId = (Map<Id, Id>)JSON.deserialize(answersString, Map<Id, Id>.class);
        System.debug(answerIdByQuestionId);
        // Map relevent questions by their Id
        Map<Id, Quiz_Question__c> questionsById = new Map<Id, Quiz_Question__c>([SELECT Id, Correct_Answer__c
                                                                                FROM Quiz_Question__c
                                                                                WHERE Id IN :answerIdByQuestionId.keySet()]);
        Map<Id, Boolean> answerMap = new Map<Id, Boolean>();
        // Iterate through each passed answer
        for(Id eachQuestionId : answerIdByQuestionId.keySet()) {
            Boolean isCorrect = false;
            // If passed answer matches Correct_Answer the user answered correctly
            if(questionsById.get(eachQuestionId).Correct_Answer__c == answerIdByQuestionId.get(eachQuestionId)) {
                isCorrect = true;
            }
            answerMap.put(eachQuestionId, isCorrect);
        }
        return answerMap;
    }

    /*
        Returns questions that are associated with the passed Question_Set Id

        Parameters:
        @   questionSetId - Id of the Question_Set whose questions we are interested in 
    */
    private static List<Quiz_Question__c> getQuestionPool(Id questionSetId)
    {
        return [SELECT Id, Body__c, Correct_Answer__r.Id, Correct_Answer__r.Name,
                        Answer_Option_A__r.Body__c, Answer_Option_B__r.Body__c,
                        Answer_Option_C__r.Body__c, Answer_Option_D__r.Body__c,
                        Answer_Option_E__r.Body__c
                FROM Quiz_Question__c
                WHERE Id IN (SELECT Quiz_Question__c
                            FROM Quiz_Questions_In_Set__c
                            WHERE Quiz_Question_Set__c = :questionSetId)
                ORDER BY CreatedDate DESC];
    }

    /*
        Returns a Quiz_Session associated with the Quiz_Participant and Quiz_Question_Set if one exists

        Parameters:
        @   participantId - Id of target Quiz_Participant
        @   questionSetId - Id of target Quiz_Question_Set
    */
    private static List<Quiz_Session__c> getExistingSession(Id participantId, Id questionSetId)
    {
        return [SELECT Id, Quiz_Participant__c, Question_Set__c, Question_Order__c
                FROM Quiz_Session__c
                WHERE Quiz_Participant__c = :participantId
                    AND Question_Set__c = :questionSetId
                ORDER BY CreatedDate DESC];
    }

    /*
        Returns a List<Integer> that simply contains the values of indices from {0 ... [list size]}, shuffled.
            Ex: size=3 => could return [1,0,2];

        Parameters:
        @   size - Length of the list to be produced
    */
    private static List<Integer> generateQuestionOrder(Integer size)
    {
        List<Integer> result = new List<Integer>();
        for(Integer i=0; i < size; i++) {
            result.add(i);
        }
        shuffleIndices(result);
        return result;
    }

    /*
        Will continue shuffling the passed List<Integer> until it produces an order that does not match its previous order

        Parameters:
        @   previousQuestionOrder - List<Integer> that contains the question order previously seen for the session
        @   newQuestionOrder - List<Integer> that is shuffled till it no longer matches previousQuestionOrder
    */
    private static void ensureIsNewOrder(List<Integer> previousQuestionOrder, List<Integer> newQuestionOrder)
    {
        Boolean isSameOrder = true;
        Integer failsafe = 0; // PLACEHOLDER
        while(isSameOrder && failsafe < 100) {
            for(Integer i=0; i < previousQuestionOrder.size(); i++) {
                if(previousQuestionOrder[i] != newQuestionOrder[i]) {
                    isSameOrder = false;
                    break;
                }
            }
            if(isSameOrder) {
                shuffleIndices(newQuestionOrder);
            }
            failsafe++;
        }
    }

    /*
        Shuffles the order of the elements in the passed List<Integer>

        Parameters:
        @   indices - List<Integer> that will be shuffled
    */
    private static void shuffleIndices(List<Integer> indices)
    {
        Integer currentIndicesIndex;
        Integer randomIndex;
        Integer currentIndex = indices.size();
     
        while (currentIndex != 0) {
            randomIndex = Integer.valueOf(Math.floor(Math.random() * currentIndex));
            currentIndex -= 1;
            currentIndicesIndex = indices[currentIndex];
            indices[currentIndex] = indices[randomIndex];
            indices[randomIndex] = currentIndicesIndex;
        }
    }

    /*
        Parses the passed String and returns it as a List<Integer>

        Parameters:
        @   questionOrder - String containing the previous question order in the following format:
                            "XX,YY, ... , ZZ"
    */
    private static List<Integer> parseQuestionOrder(String questionOrder)
    {
        List<Integer> result = new List<Integer>();
        List<String> questionIndicesString = questionOrder.split(',');
        for(String eachIndex : questionIndicesString) {
            result.add(Integer.valueOf(eachIndex));
        }
        return result;
    }

    /*
        Converts the passed List<Integer> into a String for saving to a record

        Parameters:
        @   questionOrder - List<Integer> that will be converted to a String with the following format:
                            "XX,YY, ... , ZZ"
    */
    private static String encodeQuestionOrder(List<Integer> questionOrder)
    {
        String result = '';
        for(Integer eachInt : questionOrder) {
            result += eachInt + ',';
        }
        return result.removeEnd(',');
    }

    public class QuizRequest {
        @AuraEnabled
        public String participantId {get; set;}
        @AuraEnabled
        public String questionSetId {get; set;}
    }
}
